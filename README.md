[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/ArsalaBangash/vuex-basics)

<h1 align="center">
  Vue3 VueX State Machine
</h1>

## Overview

The purpose of this repo is to experiment with Vue3, VueX, and State Machines to develop user interfaces.

## Resources

- [Vue3 with Vuex4 Tutorial Video](https://www.youtube.com/watch?v=t_VgDeUJ3_I)
- [Vue3 Docs](https://v3.vuejs.org/)
- [Vuex 4 Docs](https://vuex.vuejs.org/)
- [Vuex + State Machines Sandbox](https://codesandbox.io/s/snowy-grass-igki6?file=/src/store/todoActions.js)
- [FSMs with Vue3](https://www.youtube.com/watch?v=fT9p9CCSrn8&feature=youtu.be)
- [FSMs with Vue3 Repo](https://github.com/sarahdayan/markdown-editor-vue-xstate)


## Xstate Notes

The interpreter is responsible for interpreting the state machine/statechart and doing all of the above - that is, parsing and executing it in a runtime environment. An interpreted, running instance of a statechart is called a service.

https://xstate.js.org/docs/guides/interpretation.html

https://xstate.js.org/docs/guides/start.html

https://frontstuff.io/using-state-machines-in-vuejs-with-xstate

https://xstate.js.org/viz/

